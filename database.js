   const mongoose = require('mongoose');
   const Schema = mongoose.Schema;
   
   const bookSchema = new Schema({
            name: String,
            author: String,
            published: Boolean
   
   }, {
    versionKey: false // removed __V:0 from my collections
})


	let book = mongoose.model('book',bookSchema);
	module.exports = book;
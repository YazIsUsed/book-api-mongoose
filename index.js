
const express = require('express');
let app = express();

let mongoose = require('mongoose');
let book = require('./database.js')

mongoose.connect('mongodb://localhost:27017/book', { useNewUrlParser: true });
app.use(express.json())


let db = mongoose.connection;
   db.on('error',console.error.bind(console,'ops!! ')).once('open',function(){
   	  console.log('connection to database has been established . . ')
   });


app.post('/books',(req,res)=>{

let mybook = new book(
	{name:req.body.name,
	 author:req.body.author,
	 published:req.body.published
	})

mybook.save(function(err, book){
	if(err){
		res.json({"message":"something wrong occured !"})
	}else {
console.log(mybook);
res.status(201).json({"message":"Object has been succefully created :",book:mybook.name})
}
}); })


app.get('/books',(req,res)=>{
	book.find({}).exec(function(err,book){
		if(err){
			res.status(404).send('error 404,data not found');
		} else {
     		console.log(book)
	        res.json(book);
	    }
	});
});


app.get('/books/:id',(req,res)=>{
	  
	   book.findOne({
	   	   _id:req.params.id}
	   ).exec(function(err,book){
	   	   if(err){
	   	   	    res.send('something is wrong');
	   	   }else{
	       	   	console.log(book);
	    	   	res.json({book});
	   	   }
	   })
})

app.put('/books/:id',(req,res)=>{
	book.findOneAndReplace(
	{
		_id : req.params.id
	},
	{$set:
		{   name: req.body.name,
	        author: req.body.author,
            published: req.body.published
        }
    },
    {upsert:true},
	function(err,book){
		if (err){
			res.send('book requested not in database');
		}else{
            res.json({"message:":"succefully updated"});  
			}
		})})

app.delete('/books/:id',(req,res)=>{
	book.findOneAndDelete({
		_id:req.params.id
	},function(err,book){
		if(err){
			res.json({"message":"something went wrong"})
		}else{
			res.status(204).json({"message":"removed"})
		}
	})
})
app.listen(3000);